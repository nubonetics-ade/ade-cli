## ADE Atom

- This project generates an Atom (https://atom.io/) volume for [`ade`](https://gitlab.com/ApexAI/ade-cli)

### How to use

- In the `.aderc` file add, `registry.gitlab.com/apexai/atom:latest` to the list of `ADE_IMAGES`: e.g.

```
export ADE_IMAGES="
  registry.gitlab.com/group/project/ade:latest
  registry.gitlab.com/apexai/ade-atom:latest
"
```

**Note:** The following system dependencies must be installed in the base image
(`registry.gitlab.com/group/project/ade:latest` in the example above) for Atom to launch succesfully:
`apt install lib-gtk-3-0 x11-utils libxss1 gconf2 libnss3 libasound2 libxkbfile1`

### Installing plugins automatically

- It is possible to automatically install plugins by creating in script called `/etc/atom/atom-install-our-plugins`
in the base image
    - This script will only run the first time that the Atom volume is loaded
- For an example of a script, see the
[AutowareAuto project](https://gitlab.com/AutowareAuto/AutowareAuto/blob/master/tools/ade_image/atom-install-our-plugins)

### Getting a specific Atom version

- Generating a volume for a different Atom version is as simple as creating a git tag (e.g `v1.35.0`)
- [Create an issue](https://gitlab.com/ApexAI/ade-atom/issues/new), if the desired version is not available 
